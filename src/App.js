import logo from './logo.svg';
import './App.css';
import Guitars from './Components/Views/Guitars';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import NotFound from './Components/Views/NotFound';

function App() {






  return (
    <div>
      <BrowserRouter>
      <Routes>
        <Route path = '/' element = {<Guitars/>} /> 
        <Route path = '*' element = {<NotFound/>}/>
      </Routes>
      </BrowserRouter>
      
    </div>
  )
}

export default App;
