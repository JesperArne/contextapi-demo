import { useState, useEffect } from "react";
import GuitarList from "../Guitar/GuitarList";

const Guitars = () => {

  return( 
  <div className="App">
      <GuitarList></GuitarList>
  </div>);
};

export default Guitars;
