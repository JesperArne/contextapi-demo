const GuitarListItem = (props) => {


    const {guitar} = props
  return (
    <div>
      <h4>{guitar.model}</h4>
      <img
        src={guitar.image}
        alt="A guitar wheeping"
      ></img>

      <h3>{guitar.bodyType}</h3>
    </div>
  );
};

export default GuitarListItem;
