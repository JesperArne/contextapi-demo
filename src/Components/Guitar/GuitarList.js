import { useEffect, useState } from "react";
import GuitarListItem from "./GuitarListItem";

const GuitarList = () => {
  const [guitars, setGuitars] = useState([]);

  const handleImageClick = () => {
    console.log("guitar clicked");
  };

  useEffect(() => {
    //component did mount - instead of using the function in a button click
    handleLoadGuitars();

    // return () => {
    //     //unmounted
    //     alert("unmounted")
    // }
  }, []);

  // //mounts when some argument is called in the array/ when something is dealt with in data
  // useEffect(()=>{
  //     //component did mount
  //     console.log("state updated")

  // },[guitars])

  const models = guitars.map((guitar) => {
    return (
      <div key={guitar.id}>
        <GuitarListItem guitar = {guitar} />
      </div>
    );
  });

  // const handleImageClick=()=>{
  //     console.log("imageclicked")

  // this handles dataimnput from API without async -- below is async function
  // const handleLoadGuitars =()=>{
  //     console.log("log")
  //     fetch("https://noroff-assignment-jesper-api.herokuapp.com/guitars")
  //         .then(response => response.json())
  //         .then(result => console.log(result))
  // }

  const handleLoadGuitars = async () => {
    console.log("log");
    const response = await fetch(
      "https://noroff-assignment-jesper-api.herokuapp.com/guitars"
    );
    const result = await response.json();
    // result.forEach(guitar => {
    //     guitars.push(guitar)
    // });
    // console.log (guitars)
    setGuitars(result);
  };

  return (
    <div>
      <h1>Guitar Store</h1>
      {/* <button onClick={handleLoadGuitars}>Load</button> - handles fetch on push button*/}
      <div>{guitars.length > 1 ? models : <h2>no guitars</h2>} </div>
    </div>
  );
};

export default GuitarList;
